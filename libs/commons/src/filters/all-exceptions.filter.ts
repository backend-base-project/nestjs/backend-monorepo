import { ArgumentsHost, Catch, HttpException, HttpStatus, ExceptionFilter } from '@nestjs/common';
import { castArray } from 'lodash';

@Catch()
export class AllExceptionsFilter implements ExceptionFilter {
    catch(exception, host: ArgumentsHost) {
        const context = host.switchToHttp();
        const response = context.getResponse();
        const request: Request = context.getRequest();

        if(request && response) {
            const status: number = exception instanceof HttpException ?
                exception.getStatus() :
                HttpStatus.INTERNAL_SERVER_ERROR;
            const errMessages = castArray(exception.message);
            response.status
                .status(status)
                .json({
                    statusCode: status,
                    timestamp: new Date().toISOString(),
                    path: request.url,
                    message: errMessages.join('\n')
                })
        }
    }
}