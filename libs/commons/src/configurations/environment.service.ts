import { get, set } from 'lodash';

export class EnvironmentService {
    public static get NODE_ENV() {
        // Here we use this kind because webpack always replaces process.env.NODE_ENV with string of the NODE_ENV given on running build project
        return this.get('NODE_ENV', 'local');
    }

    public static get(envName:string, defaultValue?:any) {
        const envValue = get(process.env, envName);
        return defaultValue ? defaultValue : envValue;
    }

    public static set(envName:string, envValue:string | number | boolean){
        set(process.env, envName, envValue);
    }
}