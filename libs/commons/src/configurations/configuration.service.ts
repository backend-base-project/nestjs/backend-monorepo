import { castArray, identity } from 'lodash';
import { readEnv } from 'read-env';
import { EnvironmentService } from './environment.service';

export class ConfigurationService {
    public static get config() { return require('config'); }
    public static overridenConfigs = {};

    public static initialize(directory: string, envPrefix = 'APP') {
        const directories = castArray(directory);
        const envName = 'NODE_CONFIG_DIR';
        const finalDirectories = (directories as string[]).filter(identity);
        const finalDirectory = finalDirectories.join(':');

        this.loadEnvConfig(envPrefix);
        EnvironmentService.set(envName, finalDirectory);
    }

    public static get(key: string) {
        return this.config.get(key);
    }

    private static loadEnvConfig(envPrefix) {
        const result = readEnv(envPrefix);
        Object.assign(this.overridenConfigs, result);
    }
}