/**
 * This is not a production server yet!
 * This is only a minimal backend to get started.
 */

import { INestApplication, Logger, ValidationPipe, VersioningType } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';
import { AppModule } from './app/app.module';
import { ConfigurationService } from '@backend-monorepo/libs/commons/configurations/configuration.service';
import { EnvironmentService } from '@backend-monorepo/libs/commons/configurations/environment.service';
import { SentryInterceptor } from '@backend-monorepo/libs/commons/interceptors/sentry.interceptor';
import { AllExceptionsFilter } from '@backend-monorepo/libs/commons/filters/all-exceptions.filter';
import { NestExpressApplication } from '@nestjs/platform-express';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import * as BasicAuth from 'express-basic-auth';
import * as Sentry from '@sentry/node';
import path = require('path');

export async function bootstrap() {
  let app: INestApplication & NestExpressApplication = await NestFactory.create(AppModule);

  ConfigurationService.initialize(process.env.CONFIG_DIR ?
    process.env.CONFIG_DIR : path.resolve(path.join(__dirname, 'assets/config')));

  //TODO: testing environemnt

  if(EnvironmentService.NODE_ENV === 'production') {
    //TODO: whitelist domain
    app.enableCors({
      origin: (origin, callback) => {
        if(!origin) callback(null, true);
        callback(new Error('Not allowed by CORS'));
      }
    });
  }
  else app.enableCors();

  app.useGlobalFilters(new AllExceptionsFilter());
  app.useGlobalPipes(new ValidationPipe({ transform:true }));
  app.enableVersioning({ type:VersioningType.URI });
  app.use(
    ['/docs', '/docs-json'],
    BasicAuth({
      challenge: true,
      users: {
        [ConfigurationService.get('basicAuth.user')]: `${ConfigurationService.get('basicAuth.pass')}`
      }
    })
  )

  const sentryDSN = ConfigurationService.get('sentry.dsn');
  if(sentryDSN) {
    Sentry.init({
      dsn: ConfigurationService.get('sentry.dsn')
    });
    app.useGlobalInterceptors(new SentryInterceptor());
  }

  const options = new DocumentBuilder()
    .setTitle('AUTH API')
    .setDescription('Base Auth API')
    .setVersion('1.0')
    .build();

  const document = SwaggerModule.createDocument(app, options);
  SwaggerModule.setup('docs', app, document);

  const port = process.env.PORT || 3001;
  const server = await app.listen(port);

  Logger.log(
    `🚀 Application is running on: http://localhost:${port}`
  );
  server.timeout = 1000 * 60 * 10; //10 minutes

  return app;
}
